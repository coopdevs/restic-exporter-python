![under construction yellow black bar](http://textfiles.com/underconstruction/Atlantis8384CONSTRUCTION.gif)![under construction animation person working](http://textfiles.com/underconstruction/CoCollegeParkCenter8527imagesconstruct.gif)![under construction yellow black bar](http://textfiles.com/underconstruction/Atlantis8384CONSTRUCTION.gif)

This is a draft for a simple restic exporter. It uses an external restic binary that you need to provide to the script.

## Goal

To gain visibility of current backups at Grafana. This means:
- A list of all saved snapshots
- Graph with storage used by the restic repo
- A list with the top level dirs / files of the last snapshot.
- Graph with the storage needed to restore the last snapshot.
- A timer (?) since the last uploaded snapshot.

With this panels set, we can program some alarms:
- When too much time has passed since last snapshot
- When the restic repo uses too much storage
- When some critical files are missing
- When the latest snapshot is suspiciously small

## Assumptions
1. You are already doing (daily) backups with restic
2. You've got restic installed in the same host that this script, and
3. You have a wrapper script with the restic credentials to run it.
4. You run node exporter at the same host, so that it can read the file we generate


## Status

It's a draft, to consider it usable it needs at least:
- [ ] Properly mock sample json to test
- [ ] Use real restic data. The commented code used to work.
- [ ] Test it with node Exporter, prometheus and grafana. Can we get the panels we wanted?

## Further needed improvements

- [ ] Configurable output filepath
- [ ] Proper error handling
- [ ] Checked requirements file
- [ ] Call restic directly without an external wrapper
